#include <glut.h>
#define _USE_MATH_DEFINES

enum class DisplayMode
{
    Invalid,
    Rotate, Translate, Scale,
    RotateAroundCircle
};

enum class MenuItem
{
    Invalid,
    ObjectCube, ObjectOctahedron,
    ModeEdges, ModeSolidColor, ModeInterpolatedColor, ModeTexture,
    CircleRotateNone, CircleRotateBig, CircleRotateSmall,
    SpeedSlow, SpeedMedium, SpeedFast,
    Reset,
    Play, Pause,
    Exit
};

static DisplayMode transformMode = DisplayMode::Rotate;

static GLfloat rotateTheta[] = { 0.0, 0.0, 0.0 };
static constexpr float rotationFactor = 50.0f;

static GLfloat translateTheta[] = { 0.0, 0.0, 0.0 };
static constexpr float translateFactor = 1.0;

static GLfloat scaleTheta[] = { 1.0, 1.0, 1.0 };
static constexpr float scaleFactor = 0.5f;

static float circleRotationAngle = 0.0f;
static constexpr float circleRotationRadius = 2.0f;
static constexpr float circleRotationFactor = 50.0f;

static GLint axisToRotate = 0;
static float deltaTime = 0.0f; // in seconds

static int menuValue = (int)MenuItem::Invalid;
static int windowId;

static constexpr int textureSize = 8;
static GLubyte imageData[textureSize][textureSize][3];

static constexpr GLfloat octahedronVertices[][3] = {
    {-1.0,  0.0, -1.0}, { 1.0 ,  0.0, -1.0}, { 1.0 ,  0.0,  1.0}, { -1.0 ,  0.0,  1.0}, // flat square
    { 0.0,  1.0,  0.0}, //top 4
    { 0.0,  -1.0,  0.0} //bottom 5
};
static constexpr GLfloat octahedronColors[][3] = {
        {1.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {1.0, 0.0,01.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
};
static constexpr int octahedronIndices[][3] = {
    {0, 4, 1}, {1, 4, 2}, {2, 4, 3}, {3, 4, 0},
    {0, 5, 1}, {1, 5, 2}, {2, 5, 3}, {3, 5, 0}
};

static constexpr GLfloat cubeVertices[][3] = {
    {-1.0, -1.0, -1.0}, { 1.0, -1.0, -1.0},
    { 1.0,  1.0, -1.0}, {-1.0,  1.0, -1.0}, 
    {-1.0, -1.0,  1.0}, { 1.0, -1.0,  1.0}, 
    { 1.0,  1.0,  1.0}, {-1.0,  1.0,  1.0}
};
static constexpr GLfloat cubeColors[][3] = {
        { 0.0,  0.0,  0.0}, { 1.0,  0.0,  0.0},
        { 1.0,  1.0,  0.0}, { 0.0,  1.0,  0.0}, { 0.0,  0.0,  1.0},
        { 1.0,  0.0,  1.0}, { 1.0,  1.0,  1.0}, { 0.0,  1.0,  1.0}
};
static constexpr int cubeIndices[][4] = {
    {0, 3, 2, 1},
    {2, 3, 7, 6},
    {0, 4, 7, 3},
    {1, 2, 6, 5},
    {4, 5, 6, 7},
    {0, 1, 5, 4}
};

void myReshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (w <= h)
        glOrtho(-2.0, 2.0, -2.0 * (GLfloat)h / (GLfloat)w,
                2.0 * (GLfloat)h / (GLfloat)w, -10.0, 10.0);
    else
        glOrtho(-2.0 * (GLfloat)w / (GLfloat)h,
                2.0 * (GLfloat)w / (GLfloat)h, -2.0, 2.0, -10.0, 10.0);

    glMatrixMode(GL_MODELVIEW);
}

//void onMouseEntry(int btn, int state, int x, int y)
//{
//    if (state != GLUT_DOWN)
//        return;
//
//    switch (btn)
//    {
//    case GLUT_LEFT_BUTTON:
//    default:
//        axisToRotate = 0; break;
//    case GLUT_MIDDLE_BUTTON:
//        axisToRotate = 1; break;
//    case GLUT_RIGHT_BUTTON:
//        axisToRotate = 2; break;
//    }
//}
//
//void onKeyboardEntry(unsigned char key, int x, int y)
//{
//    switch (key)
//    {
//    case 'a':
//        transformMode = DisplayMode::Rotate; break;
//    case 's':
//        transformMode = DisplayMode::Translate; break;
//    case 'd':
//        transformMode = DisplayMode::Scale; break;
//    case 'f':
//        transformMode = DisplayMode::RotateAroundCircle; break;
//    }
//}


void generateTexture()
{
    for (int row = 0; row < textureSize; row++)
    {
        for (int col = 0; col < textureSize; col++)
        {
            if (col % 2)
            {
                imageData[row][col][0] = row % 2 ? 50 : 255;
                imageData[row][col][1] = row % 2 ? 50 : 255;
                imageData[row][col][2] = row % 2 ? 50 : 255;
            }
            else
            {
                imageData[row][col][0] = row % 2 ? 255 : 50;
                imageData[row][col][1] = row % 2 ? 255 : 50;
                imageData[row][col][2] = row % 2 ? 255 : 50;
            }

        }
    }
}


void cube(MenuItem displayMode)
{
    bool showTexture = displayMode == MenuItem::ModeTexture;
    bool showVertexColor = displayMode == MenuItem::ModeInterpolatedColor;
    bool showSolidColor = displayMode == MenuItem::ModeSolidColor;

    for (size_t i = 0; i < 6; i++)
    {
        glBegin(GL_POLYGON);

        if (showSolidColor)
            glColor3f(1.0f / (i + 1), 1.0f / (8 - i), 0.5f);
        else
            glColor3f(1.0f, 1.0f, 1.0f);

        glVertex3fv(cubeVertices[cubeIndices[i][0]]);
        if (showTexture)
            glTexCoord2f(0.0f, 0.0f);
        if (showVertexColor)
            glColor3fv(cubeColors[cubeIndices[i][0]]);

        glVertex3fv(cubeVertices[cubeIndices[i][1]]);
        if (showTexture)
            glTexCoord2f(0.0f, 1.0f);
        if (showVertexColor)
            glColor3fv(cubeColors[cubeIndices[i][1]]);

        glVertex3fv(cubeVertices[cubeIndices[i][2]]);
        if (showTexture)
            glTexCoord2f(1.0f, 1.0f);
        if (showVertexColor)
            glColor3fv(cubeColors[cubeIndices[i][2]]);

        glVertex3fv(cubeVertices[cubeIndices[i][3]]);
        if (showTexture)
            glTexCoord2f(1.0f, 0.0f);
        if (showVertexColor)
            glColor3fv(cubeColors[cubeIndices[i][3]]);

        glEnd();
    }
}

void octahedron(MenuItem displayMode)
{
    bool showTexture = displayMode == MenuItem::ModeTexture;
    bool showVertexColor = displayMode == MenuItem::ModeInterpolatedColor;
    bool showSolidColor = displayMode == MenuItem::ModeSolidColor;

    for (size_t i = 0; i < 8; i++)
    {
        glBegin(GL_POLYGON);
        
        if (showSolidColor)
            glColor3f(1.0f / (i + 1), 1.0f / (8 - i), 0.5f);
        else
            glColor3f(1.0f, 1.0f, 1.0f);

        glVertex3fv(octahedronVertices[octahedronIndices[i][0]]);
        if (showTexture)
            glTexCoord2f(0.0f, 0.0f);
        if (showVertexColor)
            glColor3fv(octahedronColors[octahedronIndices[i][0]]);

        glVertex3fv(octahedronVertices[octahedronIndices[i][1]]);
        if (showTexture)
            glTexCoord2f(0.5f, 1.0f);
        if (showVertexColor)
            glColor3fv(octahedronColors[octahedronIndices[i][1]]);

        glVertex3fv(octahedronVertices[octahedronIndices[i][2]]);
        if (showTexture)
            glTexCoord2f(1.0f, 0.0f);
        if (showVertexColor)
            glColor3fv(octahedronColors[octahedronIndices[i][2]]);

        glEnd();
    }
}


void calculateTransform(float multiplier = 1.0f)
{
    static bool translateBack = false;
    static bool scaleBack = false;

    circleRotationAngle += circleRotationFactor * deltaTime;
    if (circleRotationAngle > 360.0f)
        circleRotationAngle -= 360.0f;

    switch (transformMode)
    {
    case DisplayMode::Rotate:
        rotateTheta[axisToRotate] += rotationFactor * multiplier * deltaTime;
        if (rotateTheta[axisToRotate] > 360.0f)
            rotateTheta[axisToRotate] -= 360.0f;
        break;

    case DisplayMode::Translate:
        if (translateTheta[axisToRotate] > 1.0f)
            translateBack = true;
        else if (translateTheta[axisToRotate] < -1.0f)
            translateBack = false;
        translateTheta[axisToRotate] = translateBack 
            ? translateTheta[axisToRotate] - translateFactor * multiplier * deltaTime
            : translateTheta[axisToRotate] + translateFactor * multiplier * deltaTime;
        break;


    case DisplayMode::Scale:
        if (scaleTheta[axisToRotate] > 2.0f)
            scaleBack = true;
        else if (scaleTheta[axisToRotate] < 0.2f)
            scaleBack = false;
        scaleTheta[axisToRotate] = scaleBack 
            ? scaleTheta[axisToRotate] - scaleFactor * multiplier * deltaTime
            : scaleTheta[axisToRotate] + scaleFactor * multiplier * deltaTime;
        break;

    default:
    case DisplayMode::Invalid:
    case DisplayMode::RotateAroundCircle:
        break;
    }
}

void setGlobalTransform()
{
    glRotatef(rotateTheta[0], 1.0, 0.0, 0.0);
    glRotatef(rotateTheta[1], 0.0, 1.0, 0.0);
    glRotatef(rotateTheta[2], 0.0, 0.0, 1.0);
    glTranslatef(translateTheta[0], translateTheta[1], translateTheta[2]);
    glScalef(scaleTheta[0], scaleTheta[1], scaleTheta[2]);
}

void calculateDeltaTime()
{
    static int elapsedTimePrev;
    int elapsedTime = glutGet(GLUT_ELAPSED_TIME);
    deltaTime = (elapsedTime - elapsedTimePrev) / 1000.0f;
    elapsedTimePrev = elapsedTime;
}

void setMenuValues(MenuItem* objectToDisplay, MenuItem* rotationMode, MenuItem* displayMode, float* animationSpeed, bool* playAnimation)
{
    if (menuValue == (int)MenuItem::CircleRotateBig || menuValue == (int)MenuItem::CircleRotateSmall || menuValue == (int)MenuItem::CircleRotateNone)
        *rotationMode = (MenuItem)menuValue;

    if (menuValue == (int)MenuItem::ObjectOctahedron || menuValue == (int)MenuItem::ObjectCube)
        *objectToDisplay = (MenuItem)menuValue;

    if (menuValue == (int)MenuItem::ModeEdges || menuValue == (int)MenuItem::ModeInterpolatedColor || menuValue == (int)MenuItem::ModeSolidColor || menuValue == (int)MenuItem::ModeTexture)
        *displayMode = (MenuItem)menuValue;

    if (menuValue == (int)MenuItem::SpeedSlow)
        *animationSpeed = 0.5f;
    else if (menuValue == (int)MenuItem::SpeedMedium)
        *animationSpeed = 1.0f;
    else if (menuValue == (int)MenuItem::SpeedFast)
        *animationSpeed = 2.0f;

    if (menuValue == (int)MenuItem::Play)
        *playAnimation = true;
    else if (menuValue == (int)MenuItem::Pause)
        *playAnimation = false;

    if (menuValue == (int)MenuItem::Reset)
    {
        rotateTheta[0] = 0.0f;
        rotateTheta[1] = 0.0f;
        rotateTheta[2] = 0.0f;
        circleRotationAngle = 0.0f;
    }

    menuValue = (int)MenuItem::Invalid;
}


void display(void)
{
    static MenuItem objectToDisplay = MenuItem::Invalid;
    static MenuItem rotationMode = MenuItem::Invalid;
    static MenuItem displayMode = MenuItem::Invalid;
    static float animationSpeed = 1.0f;
    static bool playAnimation = false;

    calculateDeltaTime();
    setMenuValues(&objectToDisplay, &rotationMode, &displayMode, &animationSpeed, &playAnimation);    

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
    glPushMatrix();

    if (displayMode == MenuItem::ModeEdges)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (playAnimation)
        calculateTransform(animationSpeed);

    if (rotationMode == MenuItem::CircleRotateBig)
    {
        glRotatef(circleRotationAngle, 0.0, 0.0, 1.0);
        glTranslatef(circleRotationRadius, 0.0f, 0.0f);
    }
    else if (rotationMode == MenuItem::CircleRotateSmall)
    {
        glRotatef(circleRotationAngle, 0.0, 0.0, 1.0);
        glTranslatef(circleRotationRadius / 2.0f, 0.0f, 0.0f);
    }

    setGlobalTransform();

    if (objectToDisplay == MenuItem::ObjectOctahedron)
        octahedron(displayMode);
    else if (objectToDisplay == MenuItem::ObjectCube)
        cube(displayMode);

    glPopMatrix();
    glFlush();
    glutPostRedisplay();
    glutSwapBuffers();
}

void menuFunc(int num)
{
    if (num == (int)MenuItem::Exit)
    {
        glutDestroyWindow(windowId);
        exit(0);
    }
    menuValue = num;
    //glutPostRedisplay();
}

void createMenu(void)
{
    static int submenuObject = glutCreateMenu(menuFunc);
    glutAddMenuEntry("Cube", (int)MenuItem::ObjectCube);
    glutAddMenuEntry("Octahedron", (int)MenuItem::ObjectOctahedron);

    static int submenuDisplayMode = glutCreateMenu(menuFunc);
    glutAddMenuEntry("Edges", (int)MenuItem::ModeEdges);
    glutAddMenuEntry("Solid Color", (int)MenuItem::ModeSolidColor);
    glutAddMenuEntry("Interpolated Color", (int)MenuItem::ModeInterpolatedColor);
    glutAddMenuEntry("Texture", (int)MenuItem::ModeTexture);

    static int submenuRotation = glutCreateMenu(menuFunc);
    glutAddMenuEntry("None", (int)MenuItem::CircleRotateNone);
    glutAddMenuEntry("Small", (int)MenuItem::CircleRotateSmall);
    glutAddMenuEntry("Big", (int)MenuItem::CircleRotateBig);

    static int submenuSpeed = glutCreateMenu(menuFunc);
    glutAddMenuEntry("Slow", (int)MenuItem::SpeedSlow);
    glutAddMenuEntry("Medium", (int)MenuItem::SpeedMedium);
    glutAddMenuEntry("Fast", (int)MenuItem::SpeedFast);

    static int menu_id = glutCreateMenu(menuFunc);
    glutAddSubMenu("Object to display", submenuObject);
    glutAddSubMenu("Display mode", submenuDisplayMode);
    glutAddSubMenu("Rotation mode", submenuRotation);
    glutAddSubMenu("Animation speed", submenuSpeed);
    glutAddMenuEntry("Reset", (int)MenuItem::Reset);
    glutAddMenuEntry("Play", (int)MenuItem::Play);
    glutAddMenuEntry("Pause", (int)MenuItem::Pause);
    glutAddMenuEntry("Exit", (int)MenuItem::Exit);

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(900, 900);
    windowId = glutCreateWindow("PGK OpenGL lab2");
    createMenu();
    generateTexture();

    glutReshapeFunc(myReshape);
    glutDisplayFunc(display);
    //glutKeyboardFunc(onKeyboardEntry);
    //glutMouseFunc(onMouseEntry);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);

    glTexImage2D(GL_TEXTURE_2D, 0, 3, textureSize, textureSize, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glutMainLoop();

    return 0;
}