#include <glut.h>

void points()
{
    glPointSize(5.0f);
    glBegin(GL_POINTS);
    for (float i = -0.9f; i < 0.95f; i += 0.1f)
    {
        glVertex2f(i, 0.9f);
    }
    glEnd();
}

void lines()
{
    glLineWidth(5.0f);
    glBegin(GL_LINES);
    glVertex2f(-1.0f, 0.8f);
    glVertex2f(-0.8f, 0.8f);
    glVertex2f(-0.6f, 0.8f);
    glVertex2f(-0.4f, 0.8f);
    glEnd();
}

void lineStrip()
{
    glBegin(GL_LINE_STRIP);
    glVertex2f(-1.0f, 0.6f);
    glVertex2f(-0.5f, 0.5f);
    glVertex2f(0.5f, 0.5f);
    glVertex2f(1.0f, 0.6f);
    glEnd();
}

void triangle()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBegin(GL_TRIANGLES);
    glVertex2f(-1.0f, 0.4f);
    glVertex2f(-0.5f, 0.2f);
    glVertex2f(0.0f, 0.4f);
    glEnd();
}

void triangleStrip()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBegin(GL_TRIANGLE_STRIP);
    glVertex2f(-1.0f, 0.0f);
    glVertex2f(-0.5f, -0.2f);
    glVertex2f(0.0f, 0.0f);
    glVertex2f(0.0f, -0.2f);
    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_TRIANGLE_STRIP);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex2f(-1.0f, 0.0f);
    glVertex2f(-0.5f, -0.2f);
    glVertex2f(0.0f, 0.0f);
    glVertex2f(0.0f, -0.2f);
    glEnd();
}

void quads()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor3f(0.0f, 1.0f, 0.0f);

    glBegin(GL_QUADS);

    glVertex2f(-1.0f, -0.4f);
    glVertex2f(-1.0f, -0.3f);
    glVertex2f(-0.5f, -0.25f);
    glVertex2f(-0.5f, -0.3f);

    glVertex2f(0.0f, -0.4f);
    glVertex2f(0.0f, -0.3f);
    glVertex2f(0.5f, -0.25f);
    glVertex2f(0.5f, -0.3f);

    glEnd();
}

void quadsStrip()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor3f(1.0f, 1.0f, 1.0f);

    glBegin(GL_QUAD_STRIP);

    glVertex2f(-0.9f, -0.5f);
    glVertex2f(-0.9f, -0.45f);
    glVertex2f(-0.7f, -0.6f);
    glVertex2f(-0.7f, -0.55f);
    glVertex2f(-0.1f, -0.6f);
    glVertex2f(-0.1f, -0.55f);

    glEnd();
}

void polygon()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor3f(1.0f, 1.0f, 0.0f);

    glBegin(GL_POLYGON);

    glVertex2f(0.5f, -0.9f);
    glVertex2f(0.6f, -0.7f);
    glVertex2f(0.8f, -0.7f);
    glVertex2f(0.8f, -0.6f);
    glVertex2f(0.9f, -0.7f);
    glVertex2f(0.85f, -0.9f);

    glEnd();
}

void mydisplay()
{
    glClear(GL_COLOR_BUFFER_BIT); //czyszczenie bufora obrazu
    
    points();
    lines();
    lineStrip();
    triangle();
    triangleStrip();
    quads();
    quadsStrip();
    polygon();

    glFlush(); //wy�wietlenie bufora obrazu
}

int main(int argc, char** argv)
{
    glutInitWindowSize(1000, 1000);
    glutCreateWindow("PGK opengl lab1"); //tworzenie okna aplikacji
    glutDisplayFunc(mydisplay); //definiowanie funkcji callback do wy�wietlania zawarto�ci okna
    glutMainLoop(); //g��wna p�tla aplikacji
}